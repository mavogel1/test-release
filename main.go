package main // import "gitlab.com/mavogel/release-testing"

import (
	"fmt"
	"os"

	"github.com/xanzy/go-gitlab"
)

func main() {
	fmt.Println("bla")

	token, ok := os.LookupEnv("GITLAB_TOKEN")
	if !ok {
		panic("fail")
	}
	client := gitlab.NewClient(nil, token)
	owned := true
	projects, _, err := client.Projects.ListProjects(&gitlab.ListProjectsOptions{
		Owned: &owned,
	})
	if err != nil {
		panic(err)
	}

	for _, project := range projects {
		fmt.Printf("p: %v\n", project.ID)
	}

	projectBaseURL := "https://gitlab.com/mavogel/release-testing"
	releaseProjectPID := "12642869"
	file := "test_upload2.txt"
	// opts := gitlab.OptionFunc
	projectFile, _, err := client.Projects.UploadFile(releaseProjectPID, file, nil)
	if err != nil {
		panic(err)
	}
	fmt.Printf("file: %v\n", projectFile.URL)

	name := "v0.0.1"
	desc := "Desc"
	ref := "42e3ede10996df5fe2fcd17777bc3ce66ba1cd89"
	tageName := name
	release, _, err := client.Releases.CreateRelease(releaseProjectPID, &gitlab.CreateReleaseOptions{
		Name:        &name,
		Description: &desc,
		Ref:         &ref,
		TagName:     &tageName,
		Assets: &gitlab.ReleaseAssets{
			Links: []*gitlab.ReleaseAssetLink{
				&gitlab.ReleaseAssetLink{
					Name: file,
					URL:  projectBaseURL + projectFile.URL,
				},
			},
		},
	})

	if err != nil {
		panic(err)
	}

	fmt.Printf("r name: %v", release.Name)

}
